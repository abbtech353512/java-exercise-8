public class CreditCardFactory {
    public static CreditCard CreateCard(String cardType) {
        if (cardType == "Visa") {
            return new VisaCard();
        }
        else if (cardType == "MasterCard") {
            return new MasterCard();
        }
        else {
            throw new RuntimeException("Card type: [" + cardType + "] is not supported.");
        }
    }
}
