public class MasterCard implements CreditCard {

    @Override
    public String getCardType() {
        return "MasterCard";
    }

    @Override
    public int getCreditLimit() {
        return 30000;
    }

    @Override
    public int getAnnualCharge() {
        return 1200;
    }
}
