package Strategy;

public class CompressionContext {
    private ICompressionStrategy _compression;

    public CompressionContext(ICompressionStrategy compression) {
        this._compression = compression;
    }

    public void SetStrategy(ICompressionStrategy compression) {
        this._compression = compression;
    }

    public void CreateArchive(String compressedFileName) {
        _compression.CompressFolder(compressedFileName);
    }
}
