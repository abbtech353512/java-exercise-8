package Strategy;

public class Main {
    public static void main(String[] args) {
        var contextUsingRar = new CompressionContext
            (
                new RarCompressionStrategy()
            );

        var contextUsingZip = new CompressionContext
            (
                new ZipCompressionStrategy()
            );

        contextUsingZip.CreateArchive("ZipCompressedFile");
        contextUsingRar.CreateArchive("RarCompressedFile");
    }
}

