package Strategy;

public interface ICompressionStrategy {
    void CompressFolder(String compressedFileName);
}
