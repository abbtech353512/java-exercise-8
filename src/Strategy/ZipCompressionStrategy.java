package Strategy;

public class ZipCompressionStrategy implements ICompressionStrategy {

    @Override
    public void CompressFolder(String compressedFileName) {
        System.out.println("Folder compressed to \"" + compressedFileName + ".zip\" file.");
    }
}
