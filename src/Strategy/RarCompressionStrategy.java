package Strategy;

public class RarCompressionStrategy implements ICompressionStrategy {

    @Override
    public void CompressFolder(String compressedFileName) {
        System.out.println(
                "Folder compressed to \"" + compressedFileName + ".rar\" file."
        );
    }
}
