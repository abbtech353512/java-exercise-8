package Abstract_Factory;

public class Main {
    public static void main(String[] args) {
        IVehicleFactory regVFac = new RegularVehicleFactory();
        IVehicleFactory spVFac = new SportsVehicleFactory();

        IBike regularBike = regVFac.CreateBike();
        IBike sportsBike = spVFac.CreateBike();

        ICar regularCar = regVFac.CreateCar();
        ICar sportsCar = spVFac.CreateCar();

        regularBike.getDetails();
        regularCar.getDetails();
        sportsBike.getDetails();
        sportsCar.getDetails();
    }
}

