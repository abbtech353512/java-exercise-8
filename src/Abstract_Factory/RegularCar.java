package Abstract_Factory;

public class RegularCar implements ICar {

    @Override
    public void getDetails() {
        System.out.println("RegularCar details...");
    }
}
