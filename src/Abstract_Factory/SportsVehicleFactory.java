package Abstract_Factory;

public class SportsVehicleFactory implements IVehicleFactory {

    @Override
    public IBike CreateBike() {
        return new SportsBike();
    }

    @Override
    public ICar CreateCar() {
        return new SportsCar();
    }
}
