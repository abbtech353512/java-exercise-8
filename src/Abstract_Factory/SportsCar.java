package Abstract_Factory;

public class SportsCar implements ICar {

    @Override
    public void getDetails() {
        System.out.println("SportsCar details...");
    }
}
