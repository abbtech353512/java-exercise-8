package Abstract_Factory;

public class SportsBike implements IBike {

    @Override
    public void getDetails() {
        System.out.println("SportsBike details...");
    }
}
