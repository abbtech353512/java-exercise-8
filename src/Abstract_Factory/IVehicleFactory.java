package Abstract_Factory;

public interface IVehicleFactory {
    IBike CreateBike();
    ICar CreateCar();
}
