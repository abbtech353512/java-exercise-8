package Abstract_Factory;

public class RegularBike implements IBike {

    @Override
    public void getDetails() {
        System.out.println("RegularBike details...");
    }
}
