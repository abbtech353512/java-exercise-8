package Abstract_Factory;

public class RegularVehicleFactory implements IVehicleFactory {

    @Override
    public IBike CreateBike() {
        return new RegularBike();
    }

    @Override
    public ICar CreateCar() {
        return new RegularCar();
    }
}
