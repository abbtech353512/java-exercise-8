public interface CreditCard {
    String getCardType();
    int getCreditLimit();
    int getAnnualCharge();
    default void printDetails() {
        System.out.println(getCardType() +
                "[limit:" + getCreditLimit() + " | annual-charge: "
                + getAnnualCharge() + "]");
    }
}
