public class VisaCard implements CreditCard {

    @Override
    public String getCardType() {
        return "Visa";
    }

    @Override
    public int getCreditLimit() {
        return 15000; // can be configured. const is given for only example.
    }

    @Override
    public int getAnnualCharge() {
        return 500;
    }
}
